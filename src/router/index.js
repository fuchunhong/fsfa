import App from '../App.vue'

const loginChange = r => require.ensure([], () => r(require('../components/login-change/login-change.vue')), 'login-change')
const login = r => require.ensure([], () => r(require('../components/login/login.vue')), 'login')
const loginFirst = r => require.ensure([], () => r(require('../components/login-first/login-first.vue')), 'login-first')
const loginPassword = r => require.ensure([], () => r(require('../components/login-password/login-password.vue')), 'login-password')
const main = r => require.ensure([], () => r(require('../components/main/main.vue')), 'main')
const exportPlan = r => require.ensure([], () => r(require('../components/main/visit-plan/export-plan/exprot-plan.vue')), 'export-plan')
// Vue.use(Router)

export default [{
  path: '/',
  component: App,
  children: [
    {
      path: '',
      redirect: '/login'
    },
    {
      path: 'login',
      component: login
    },
    {
      path: 'login-change',
      component: loginChange
    },
    {
      path: 'login-first',
      component: loginFirst
    },
    {
      path: 'login-password',
      component: loginPassword
    },
    {
      path: 'main',
      component: main,
      children: [
        {
          path: '',
          redirect: 'export-plan'
        },
        {
          path: 'export-plan',
          component: exportPlan
        }
      ]
    }
  ]
}]

// // import Login from '@/components/login-first/login-first.vue'
// import Login from '@/components/login/login.vue'
// // import Login from '@/components/login-change/login-change.vue'
// // import Login from '@/components/login-password/login-password.vue'
//
// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'Login',
//       component: Login
//     }
//   ]
// })
