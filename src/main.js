// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
import routes from './router/index'
import VueCookie from 'vue-cookie'

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.use(VeeValidate)
Vue.use(VueCookie)
const router = new VueRouter({
  routes
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
