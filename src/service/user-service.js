import HttpService from './http-service'
export default {
  http: new HttpService(),
  // 登录
  login (option) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.http.url}user/login`, option)
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  verify (option) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.http.url}vue/forgetPW`, option)
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  sendMsg (option) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.http.url}user/sendMessage`, option)
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  updatePW (option) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.http.url}user/forgetPW`, option)
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
