import axios from 'axios'
import VueCookie from 'vue-cookie'
axios.interceptors.request.use(config => {
  console.log('http config', config)
  const token = VueCookie.get('token')
  if (token) {
    config.headers.token = token
  }
  return config
}, error => {
  console.log('error', error)
})
export default class HttpService {
  // url = 'http://localhost:8180/'
  url = 'http://localhost:1337/localhost:8180/'
  get (url, param) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url: url,
        params: param
      }).then(res => { resolve(res) })
        .catch(error => { reject(error) })
    })
  }
  post (url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: url,
        data: data
      }).then(res => { resolve(res) })
        .catch(error => { reject(error) })
    })
  }
  put (url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'put',
        url: url,
        data: data
      }).then(res => { resolve(res) })
        .catch(error => { reject(error) })
    })
  }
}
