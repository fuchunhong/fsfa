import user from '../../service/user-service'
export default {
  data () {
    return {
      errorTip: false,
      loginForm: {
        sieble_uid: '',
        password: ''
      }
    }
  },
  created () {
    this.loginForm.sieble_uid = this.$route.query.sieble_uid
  },
  methods: {
    login () {
      // 验证是否通过
      this.$validator.validateAll().then(res => {
        if (res) {
          user.login(this.loginForm)
            .then(res => {
              const data = res.data
              console.log('res', data.code)
              if (data.code === 0) {
                this.$cookie.set('token', data.result.token, { expires: '1Y' })
                // 跳转到首页
              } else if (data.code === 50011) {
                this.$router.push({path: 'login-first', query: {sieble_uid: this.loginForm.sieble_uid}})
                this.alertTip = data.msg
              } else {
                this.errorTip = data.msg
              }
            })
        }
      })
    },
    forgetPW () {
      this.$router.push({path: 'login-change'})
    }
  }
}
