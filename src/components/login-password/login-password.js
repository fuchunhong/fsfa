import user from '../../service/user-service'
export default {
  data () {
    return {
      flag: false,
      errorTip: false,
      passwordCode: {
        new_password: '',
        password_confirm: '',
        sieble_uid: '',
        verify_code: ''
      }
    }
  },
  created () {
    this.passwordCode.sieble_uid = this.$route.query.sieble_uid
    this.passwordCode.verify_code = this.$route.query.verify_code
  },
  methods: {
    backIndex () {
      this.$router.push({path: 'login'})
    },
    updatePW () {
      this.$validator.validateAll().then(res => {
        if (res) {
          user.updatePW(this.passwordCode)
            .then(res => {
              const data = res.data
              if (data.code !== 0) {
                this.errorTip = data.msg
              } else {
                this.$message({
                  message: '密码重置成功',
                  type: 'success',
                  center: true
                })
                this.$router.push({path: 'login-password', query: {sieble_uid: this.passwordCode.sieble_uid}})
              }
            })
        }
      })
    }
  }
}
