import user from '../../service/user-service'
export default {
  data () {
    return {
      show: true,
      count: '',
      timer: null,
      errorTip: false,
      loginCode: {
        sieble_uid: '',
        verify_code: ''
      }
    }
  },
  created () {
    this.loginCode.sieble_uid = this.$route.query.sieble_uid
  },
  methods: {
    verify () {
      // 验证是否通过
      this.$validator.validateAll().then(res => {
        if (res) {
          user.verify(this.loginCode)
            .then(res => {
              console.log('res', res)
              const data = res.data
              if (data.code !== 0) {
                this.errorTip = data.msg
              } else {
                this.$router.push({path: 'login-password', query: {sieble_uid: this.loginCode.sieble_uid, verify_code: this.loginCode.verify_code}})
              }
            })
        }
      })
    },
    sendMsg () {
      // console.log('fsdf', this.errors.has('sieble_uid'))
      user.sendMsg(this.loginCode)
        .then(res => {
          console.log('res', res)
          const data = res.data
          if (data.code === 0) {
            console.log('res', res)
          } else {
            this.errorTip = data.msg
          }
          const TIME_COUNT = 60
          if (!this.timer) {
            this.count = TIME_COUNT
            this.show = false
            this.timer = setInterval(() => {
              if (this.count > 0 && this.count <= TIME_COUNT) {
                this.count--
                console.log('count', this.count)
              } else {
                this.show = true
                clearInterval(this.timer)
                this.timer = null
              }
            }, 1000)
          }
        })
    },
    backIndex () {
      // this.$router.push({path: 'login', query: {sieble_uid: this.loginCode.sieble_uid}})
      this.$router.go(-1)
    }
  }
}
